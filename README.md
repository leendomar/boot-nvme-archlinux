#kernel archiso 5.7.6-arch1-1
    After upgrade to kernel 5.7.8 and lts-5.4.51 linux is not detecting nvme drive and won't boot with both linux and linux-lts


- Copy files to the root

        /etc/
            mkinitcpio.d/
                archiso.preset

        /boot/
            vmlinuz-archiso
            
        /usr/
            lib/
                modules/
                    5.7.6-arch1-1
                        ...
                        ...
                        ...

        

 - Run to make initramfs-archiso.img, initramfs-archiso-fallback.img

        # mkinitcpio -p archiso

        
 - Update grub

        # grub-mkconfig -o /boot/grub/grub.cfg


 - Success
        I have found perhaps a quick alternative that I wish is temporary.
        
        # uname -a
        Linux archlinux 5.7.6-arch1-1 #1 SMP PREEMPT Thu, 25 Jun 2020 00:14:47 +0000 x86_64 GNU/Linux

